package amazonupload.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class ConversionUtil
{
	public static byte[] readAudioFile(String filename)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try
		{
			InputStream in = new FileInputStream(filename);
			int next = in.read();
			while (next > -1)
			{
				bos.write(next);
				next = in.read();
			}
			bos.flush();
		}
		catch (Exception e)
		{
		}
		System.out.println("size " + bos.size());
		return bos.toByteArray();
	}

	public static AudioInputStream decodeAudioFile(byte[] audio)
	{
		AudioInputStream audioInputStream = null;
		try
		{
			// decodebase64
			//byte[] decodedAudio = Base64EncoderDecoder.decode(audio);
			// convert to wav file
			InputStream input = new ByteArrayInputStream(audio);
			audioInputStream = AudioSystem.getAudioInputStream(input);
			AudioFormat.Encoding targetEncoding = AudioFormat.Encoding.PCM_SIGNED;
			audioInputStream = AudioSystem.getAudioInputStream(targetEncoding, audioInputStream);
			audioInputStream = AudioSystem.getAudioInputStream(AudioFormat.Encoding.ULAW, audioInputStream);

		}
		catch (Exception e)
		{
			System.out.println("------ Exception raised from decodeAudioFile from ConvertionUtil class "+e+" ---------");
			e.printStackTrace();
		}
		return audioInputStream;
	}
}
