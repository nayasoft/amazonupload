package amazonupload;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;






import amazonupload.util.ConversionUtil;


public class AmazonAudioUpload implements AmazonUploadable
{

	Properties configProp = new Properties();
	Logger logger=Logger.getLogger(AmazonAudioUpload.class);
	Date date = null;
	DateFormat dateFormat = new SimpleDateFormat("MMM-dd-yyyy h:m:s a");
	
	private static final String AMAZON_ACCESS_KEY = "AKIAIJB3DPRKJHMWIEFQ";
    private static final String AMAZON_SECRET_KEY = "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";
   // private static final String S3_BUCKET_NAME = "Musicnote";
	public AmazonAudioUpload()
	{
		InputStream in = this.getClass().getResourceAsStream("/resources/uploadMessages.properties");

		try
		{
			if (in != null)
				configProp.load(in);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	File audioFile = null;

/*	public String writeFile(HttpServletRequest request,String folderName,String uploadPath)
	{
		date=new Date();
		String uploadStatus = null;
		String userId = null;
		String fileName="";
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		logger.info("isMultipart : "+isMultipart);
		if (isMultipart)
		{
			AmazonFileUpload fileUpload=new AmazonFileUpload();
			fileName=fileUpload.writeFile(request, "audio",uploadPath);
			uploadStatus = "Successfully uploaded" + "&" + fileName;
		}
		else
		{
			try
			{
				if (request.getAttribute("userId") != null && !request.getAttribute("userId").equals(""))
				{
					userId = (String) request.getAttribute("userId");

					logger.info("content type : " + request.getContentType());

					BufferedInputStream bufferedInputStream = new BufferedInputStream(request.getInputStream());
					logger.info("fromClient : " + bufferedInputStream);

					byte[] buff = new byte[8000];

					int bytesRead = 0;

					ByteArrayOutputStream bao = new ByteArrayOutputStream();

					while ((bytesRead = bufferedInputStream.read(buff)) != -1)
					{
						bao.write(buff, 0, bytesRead);
					}

					byte[] data = bao.toByteArray();

					AudioInputStream audioInputStream = ConversionUtil.decodeAudioFile(data);

					File fDir = null;

					if (request.getRequestURL().toString().contains(configProp.getProperty("localhost").toString()))
					{
						fDir = new File(configProp.getProperty("localPathAudio").toString() + userId);
						fDir.mkdir();
						if (request.getAttribute("fileName") != null)
						{
							fileName = request.getAttribute("fileName") + "_" + dateFormat.format(date) + ".wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						else
						{
							fileName =  dateFormat.format(date) + "_" + "audio.wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						audioFile = new File(fDir, fileName);

					}
					else
					// this parts writes to the server
					{
						fDir = new File(uploadPath + userId + "/files/");
						fDir.mkdirs();
						if (request.getAttribute("fileName") != null)
						{
							fileName = request.getAttribute("fileName") + "_" + dateFormat.format(date) +".wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						else
						{
							fileName = dateFormat.format(date) + "_" + "audio.wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						audioFile = new File(fDir, fileName);
					}
					OutputStream outputStream = new FileOutputStream(audioFile);

					AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, outputStream);

					// To save the current recording
					File audioFile1 = new File(fDir, fileName);
					OutputStream outputStream1 = new FileOutputStream(audioFile1);
					AudioInputStream audioInputStream1 = ConversionUtil.decodeAudioFile(data);
					AudioSystem.write(audioInputStream1, AudioFileFormat.Type.WAVE, outputStream1);

					uploadStatus = "Successfully uploaded" + "&" + fileName;
				}
				else
					uploadStatus = "User id is either null (or) empty";
			}
			catch (Exception e)
			{
				logger.error("----------- Exception arised from writeFile of VcAudioUpload : " + e + " -----------");
			}
		}
		return uploadStatus;
	}
*/
	public String amazonUpload(HttpServletRequest request,String bucketName) {
		String S3_BUCKET_NAME =bucketName;
		date=new Date();
		DateFormat dateFormat1 = new SimpleDateFormat("MMM-dd-yyyy");
		String uploadStatus = null;
		String userId = null;
		String fileName="";
	    String filePath=null;
	    String fileDate=null;
		String folderDate=null;
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		logger.info("isMultipart : "+isMultipart);
		if (isMultipart)
		{
			AmazonFileUpload fileUpload=new AmazonFileUpload();
			fileName=fileUpload.amazonUpload(request,bucketName);
			uploadStatus = fileName;
		}
		else
		{
			try
			{
				if (request.getAttribute("userId") != null && !request.getAttribute("userId").equals(""))
				{
					userId = (String) request.getAttribute("userId");

					logger.info("content type : " + request.getContentType());

					BufferedInputStream bufferedInputStream = new BufferedInputStream(request.getInputStream());
					logger.info("fromClient : " + bufferedInputStream);

					byte[] buff = new byte[8000];

					int bytesRead = 0;

					ByteArrayOutputStream bao = new ByteArrayOutputStream();

					while ((bytesRead = bufferedInputStream.read(buff)) != -1)
					{
						bao.write(buff, 0, bytesRead);
					}

					byte[] data = bao.toByteArray();

					AudioInputStream audioInputStream = ConversionUtil.decodeAudioFile(data);

					File fDir = null;

					if (request.getRequestURL().toString().contains(configProp.getProperty("localhost").toString()))
					{
						fDir = new File(configProp.getProperty("localPathAudio").toString() + userId);
						fDir.mkdir();
						if (request.getAttribute("fileName") != null)
						{
							fileName = request.getAttribute("fileName") + "_" + dateFormat.format(date) + ".wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						else
						{
							fileName =  dateFormat.format(date) + "_" + "audio.wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						audioFile = new File(fDir, fileName);

					}
					else
					// this parts writes to the server
					{
						fDir = new File("usr/share/Musicnote/" + userId + "/files/");
						fDir.mkdirs();
						if (request.getAttribute("fileName") != null)
						{
							fileName = request.getAttribute("fileName") + "_" + dateFormat.format(date) +".wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						else
						{
							fileName = dateFormat.format(date) + "_" + "audio.wav";
							fileName=StringUtils.deleteWhitespace(fileName);
						}
						audioFile = new File(fDir, fileName);
						logger.info("audioFile " + audioFile);
						logger.info("audioFile.getAbsolutePath() " + audioFile.getAbsolutePath());
					}
					OutputStream outputStream = new FileOutputStream(audioFile);

					AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, outputStream);

					// To save the current recording
					File audioFile1 = new File(fDir, fileName);
					OutputStream outputStream1 = new FileOutputStream(audioFile1);
					AudioInputStream audioInputStream1 = ConversionUtil.decodeAudioFile(data);
					AudioSystem.write(audioInputStream1, AudioFileFormat.Type.WAVE, outputStream1);
					
					   BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);
						
						System.out.println("awsCredentials===>"+awsCredentials);
			       	    AmazonS3 s3client = new AmazonS3Client(awsCredentials);
			       		System.out.println("s3client===>"+s3client);
			       	  
			              String keyName=fileName;
			              folderDate=dateFormat1.format(date);
			         
			                s3client.putObject(new PutObjectRequest(S3_BUCKET_NAME+"/"+userId+"/"+"files/"+folderDate,fileName,audioFile1));
			             
			                s3client.setObjectAcl(S3_BUCKET_NAME+"/"+userId+"/"+"files/"+folderDate, fileName, CannedAccessControlList.PublicRead);
			             
			                 filePath=S3_BUCKET_NAME+"/"+userId+"/"+"files/"+folderDate+"/"+fileName;
			                 logger.info("filePath " + filePath);
					
					logger.info("audioFile1 " + audioFile1.getAbsolutePath());

					uploadStatus = filePath;
					logger.info("uploadStatus " + uploadStatus);
				}
				else
				{
					uploadStatus = filePath;
				}
			}
			catch (Exception e)
			{
				logger.error("----------- Exception arised from writeFile of VcAudioUpload : " + e + " -----------");
			}
		}
		return uploadStatus;
		}

	public String deleteFile(String fullPath, String fileName) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
