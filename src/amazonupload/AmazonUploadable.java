package amazonupload;

import javax.servlet.http.HttpServletRequest;

public interface AmazonUploadable
{
	//String writeFile(HttpServletRequest request, String folderName, String uploadPath);
	String amazonUpload(HttpServletRequest request,String bucketName);
	String deleteFile(String fullPath,String fileName);

}
